from django.contrib import admin

# Register your models here.
from home.models import *

class autorAdmin(admin.ModelAdmin):
    list_display = ('nombre','apellido','id')
    search_fields = ('nacionalidad','nombre')

admin.site.register(autores,autorAdmin)
admin.site.register(Libros)
admin.site.register(cliente)
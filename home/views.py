from django.shortcuts import render
from django.views.generic import ListView,CreateView,UpdateView,DeleteView

# Create your views here.
from home.models import *

def inicio(request):
    return render(request, 'inicio.html')

class listaAutores(ListView):
    template_name = 'autores.html'
    model=autores
    context_object_name = 'autor'

class listaLibrosAutor(ListView):
    template_name = 'Libros_Autor.html'
    context_object_name = 'libros_autor'
    def get_queryset(self):
        #identificar el autor
        id=self.kwargs['pk']
        #filtrar por id
        lista=Libros.objects.filter(
            autor=id
        )
        #devuelve lista
        return lista

class listaBuscarAutores(ListView):
    template_name = 'Libros_Autor.html'
    context_object_name = 'libros_autor'
    def get_queryset(self):
        #identificar el autor
        id=self.kwargs['pk']
        #filtrar por id
        lista=Libros.objects.filter(
            autor=id
        )
        #devuelve lista
        return lista

class addAutor(CreateView):
    template_name = 'addAutores.html'
    model=autores
    fields = ['nombre','apellido','nacionalidad']
    success_url = 'autoresVista/'

class updateAutores(UpdateView):
    template_name = 'updateAutores.html'
    model = autores
    fields = ['nombre', 'apellido', 'nacionalidad']
    success_url = '/autoresVista/'

class deleteAutores(DeleteView):
    template_name = 'delAutores.html'
    model = autores
    success_url = '/autoresVista/'


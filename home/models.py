from django.db import models

# Create your models here.
class persona(models.Model):
    nombre=models.CharField('Nombre',max_length=150,null=True)
    apellido= models.CharField('Apellido', max_length=150, null=True)
    nacionalidad=models.CharField("Nacionalidad",max_length=20)
    class Meta:
        abstract=True

class autores(persona):
    def __str__(self):
        return self.nombre



class cliente(persona):
    rut= models.CharField("Rut",max_length=15,null=False,blank=False,primary_key=True)
    direccion=models.TextField("Dirección",max_length=200,blank=True)
    telefono=models.CharField("Teléfono",max_length=10,blank=False,null=False)
    def __str__(self):
        return self.rut+'-'+self.nombre

class Libros(models.Model):
    isbn=models.CharField("ISBN",max_length=7,null=False,primary_key=True)
    titulo=models.CharField("Titulo",max_length=100,null=True)
    autor=models.ForeignKey(autores,on_delete=models.CASCADE)
    def __str__(self):
        return self.titulo

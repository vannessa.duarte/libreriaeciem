from django.urls import path

from home.views import *
from home import views

urlpatterns = [
    path('', inicio,name='inicio'), #Este es el enlace al inicio
    path('autoresVista/',views.listaAutores.as_view(),name='listaAutores'),
    path('List_Libros_autor/<pk>/',views.listaLibrosAutor.as_view(),name='listaLibrosAutor'),
    path('addAutor',views.addAutor.as_view(),name='addAutores'),
    path('actualizar_autor/<pk>/',views.updateAutores.as_view(),name='upAutores'),
    path('borrar_Autor/<pk>',views.deleteAutores.as_view(),name='delAutores'),
]